export class Person {
    constructor(private name:string,
                private age:number,
                private id?:number){}

    getName(){
        return this.name;
    }
    getAge() {
        return this.age;
    }
    getId() {
        return this.id;
    }
    setId(id:number) {
        this.id=id;
    }
}