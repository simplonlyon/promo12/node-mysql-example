
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { DaoPerson } from './dao/DaoPerson';
import { Person } from './entity/Person';

const dao = new DaoPerson();

const app = express();


app.use(bodyParser.json());

app.get('/person', async (req,resp) => {
    let persons = await dao.findAllPerson();
    resp.json(persons);
});
app.post('/person', async (req,resp) => {
    let person = new Person(req.body.name, req.body.age);
    await dao.addPerson(person);
    resp.json(person);
});


app.listen(3000, () => {
    console.log('listening on 3000');
});